import React from 'react';

const Loader = (props) =>  {
  return (
    <div style={{verticalAlign: 'middle'}}>
      <div style={{ lineHeight: '45vh', textAlign: 'center', color: '#CCCCCC'}}>
        { props.children }
      </div>
    </div>
  )
}

export default Loader;
