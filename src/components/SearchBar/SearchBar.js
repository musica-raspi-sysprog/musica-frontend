import React, {useState} from 'react';
import styled from 'styled-components';
import searchIcon from './../../assets/search-icon.svg';

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  padding-bottom: 1.5em;
  width: 90vw;
`

const ConstrainedInvertedImage = styled.img`
  transition: 0.25s all ease-in-out;
  min-width: 1.15em;
  min-height: 1.15em;
`

const ConstrainedInput = styled.input`
  min-width: calc(100% - 5em);
  border-radius: 25px;
  padding: 0em 1em;
  font-size: 0.5em;
`

const SearchButtonContainer = styled.div`
  background-color: white;
  height: 1.5em;
  width: 1.5em;
  border-radius: 50%;
  padding: 0.2em;
  transition: 0.25s all ease-in-out;

  &:hover {
    background-color: #303030;
  }

  &:hover img {
    -webkit-filter: invert(1); /* safari 6.0 - 9.0 */
            filter: invert(1);
  }
`

const SearchBar = ({ onSubmit }) => {
  const [searchValue, setSearchValue] = useState('');

  const handleOnChange = (event) => {
    setSearchValue(event.target.value);
  }
  
  const submitSearchQuery = () => {
    if (searchValue) {
      onSubmit(searchValue);
    }
  }

  const handleEnterPress = (event) => {
    let code = event.keyCode || event.which;
    if (code === 13) {
      submitSearchQuery();
    }
  }

  return (
    <Container>
      <ConstrainedInput 
        name="search"
        type="search"
        placeholder="What mood are you into?"
        onChange={handleOnChange}
        onKeyUp={handleEnterPress}
        value={searchValue}
        autoFocus
      />
      <SearchButtonContainer>
        <ConstrainedInvertedImage src={searchIcon} alt="Search" onClick={() => submitSearchQuery()}/>
      </SearchButtonContainer>
    </Container>
  );
}

export default SearchBar;
