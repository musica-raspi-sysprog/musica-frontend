import styled from 'styled-components';

export const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: rgba(0, 0, 0);
  padding-bottom: 0.25em;
  padding-right: 1em;
`

export const SingleSong = styled.div`
  background-color: rgba(0, 0, 0, 0.3);
  display: flex;
  justify-content: space-between;
  transition: 0.5s all ease-in-out;

  &:hover {
    background-color: rgba(26, 26, 26, 0.7);
    box-shadow: inset 0 0 2px grey; 
  }
`

export const SongDetailContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 0.1em 0;
`

export const Thumbnail = styled.img`
  width:3.5em;
  height:3.5em;
  margin: 0 0.5em 0 0;
`

export const ScrollableContainer = styled.div`

  height: calc(100vh - 12em - 100px);
  overflow-y: scroll;

  ::-webkit-scrollbar {
    width: 10px;
  }
  
  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 3px grey; 
    border-radius: 10px;
  }
   
  ::-webkit-scrollbar-thumb {
    background: #1A1A1A; 
    box-shadow: inset 0 0 3px grey; 
    border-radius: 10px;
  }
  
  ::-webkit-scrollbar-thumb:hover {
    background: #3B3B3B; 
  }
`

export const ModButtonContainer = styled.div`
  margin: 1em;
  display: flex;
  flex-direction: row;
  position: relative;
`
export const ModButton = styled.img`
  z-index: 100;
  width:1em;
  -webkit-filter: invert(1); /* safari 6.0 - 9.0 */
          filter: invert(1);
  margin: 0 1em;
`

export const dummyQueue = [
  {
    "id": "1",
    "title": "QUEUE1",
    "author": "AUTHOR",
    "thumbnail": "",
    "videoId": "",
  },
  {
    "id": "2",
    "title": "QUEUE2",
    "author": "AUTHOR",
    "thumbnail": "",
    "videoId": "",
  },
  {
    "id": "3",
    "title": "QUEUE3",
    "author": "AUTHOR",
    "thumbnail": "",
    "videoId": "",
  },
  {
    "id": "4",
    "title": "QUEUE4",
    "author": "AUTHOR",
    "thumbnail": "",
    "videoId": "",
  }
];
