import React from 'react';
import styled from 'styled-components';

const StyledHeader = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  width: 100vw;
  background-color: #00051c;
  color: white;
  flex-direction: column-reverse;
  padding: 0.25em;
  font-style: italic;
  font-size: 0.8em;
  height: 1em;
`

const AppHeader = () => (
  <StyledHeader>@Musica</StyledHeader>
)

export default AppHeader;