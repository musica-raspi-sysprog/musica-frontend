import React, { useState, useEffect, useRef, useReducer } from 'react';
import deepcopy from 'deepcopy';
import {
  Header,
  ModButton,
  ModButtonContainer,
  ScrollableContainer,
  SingleSong,
  SongDetailContainer,
  Thumbnail,
} from '../../StyledComponents.js';
import Loader from './../../Loader/Loader.js';

import styled from 'styled-components';
import defaultThumbnail from './../../../assets/default-thumbnail.jpeg';
import substractButton from './../../../assets/substract.svg';

const Starter = styled.div`
  transition: all 0.4s ease-in-out;

  &:hover #play-button {
    transform: translateX(0.5em);
  }
`

function queueReducer(oldQueue, action) {
  switch(action.type) {
    case 'replace':
      return action.newQueue;
    case 'remove':
      return oldQueue.filter((song) => {
        return song.queueId !== action.queueId;
      });
    default:
      throw new Error();
  }
}

const Queue = (props) => {
  const [queue, queueDispatch] = useReducer(queueReducer, props.queue);
  const queueRef = useRef();

  useEffect(() => {
    const propsQueue = deepcopy(props.queue);
    // console.log(propsQueue);

    // Compare current queue with backend
    const newQueue = deepcopy(queue).map((song) => {
      let index = 0;
      while (index < propsQueue.length && propsQueue[index].queueId !== song.queueId) index++;
      if (index >= props.queue.length) {
        song.isRemoved = true;
      } else {
        song.isRemoved = false;
      }
      return song;
    });

    console.log("new", newQueue);

    // Add new song from backend
    queueDispatch({
      type: "replace",
      newQueue: propsQueue.reduce((result, song) => {
        let index = 0;
        while (index < newQueue.length && newQueue[index].queueId !== song.queueId) index++;
        if (index >= newQueue.length) {
          song.isRemoved = false
          result.push(song);
        }
        return result;
      }, newQueue)
    });
  }, [props.queue]); 

  const removeSong = (id) => {
    props.removeFromQueue(id);
  }

  useEffect(() => {
    if (!queueRef.current) {
      return ;
    }

    const handleTransition = (e) => {
      if (e.srcElement.getAttribute("data-type") === "removable") {
        const removedId = parseInt(e.srcElement.getAttribute("data-id"));
        queueDispatch({type: "remove", queueId: removedId});
      }
    }
    
    queueRef.current.addEventListener("transitionend", handleTransition);
    return () => {
      queueRef.current.removeEventListener("transitionend", handleTransition);
    };

  }, []);

  return (
    <div>
      <Header>
        <div style={{backgroundColor: '#000000'}}>
          { queue.length 
            ? <div>
              On Queue <i className="fa fa-angle-down"></i>
            </div>
            : <div>
              start searching <i className="fa fa-angle-up"></i>
            </div>
          }
        </div>
      </Header>
      <ScrollableContainer ref={queueRef}>
        {queue.length ? (
          <div style={{overflow: 'hidden', transition: 'all 0.5s ease-in-out'}}>
            {queue.map((song, index) => (
              <div
                key={song.queueId}
                data-id={song.queueId}
                data-type={queue[index].isRemoved? "removable": "unremovable"}
                style={{ 
                  transition: '0.5s all ease-in-out',
                  opacity: queue[index].isRemoved ? '0' : '1',
                  transform: queue[index].isRemoved ? 'translateX(-100%)' : '',
                  maxHeight: queue[index].isRemoved ? '0' : '3.5em',
                }}
              >
                <SingleSong 
                  className='single-song'
                  data-index={index}
                >
                  <div style={{display:'flex', justifyContent:'flex-start'}}>
                    <Thumbnail src={song.thumbnail ? song.thumbnail : defaultThumbnail} alt="song-thumbnail" />
                    <SongDetailContainer>
                      <p style={{fontSize:'1em'}}>{song.title}</p>
                      <p style={{fontSize:'0.6em'}}>{song.author}</p>
                    </SongDetailContainer>
                  </div>
                  <ModButtonContainer>
                    <ModButton src={substractButton} 
                      onClick={() => removeSong(song.queueId)}
                    />
                  </ModButtonContainer>
                </SingleSong>
              </div>
            ))}
          </div>
        ) : (
          <Loader>
            Nothing on queue
          </Loader>
        )}
    </ScrollableContainer>
    </div>
  )
}

export default Queue;
