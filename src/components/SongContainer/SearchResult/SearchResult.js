import React , { useState, useEffect } from 'react';
import {
  Header,
  ModButton,
  ModButtonContainer,
  ScrollableContainer,
  SingleSong,
  SongDetailContainer,
  Thumbnail,
} from '../../StyledComponents.js';
import Loader from './../../Loader/Loader.js';

import addButton from './../../../assets/add.svg';
import defaultThumbnail from './../../../assets/default-thumbnail.jpeg';

const SearchResult = (props) => {
  const[songs, setSongs] = useState([]);
  
  useEffect(() => {
    setSongs(props.searchResult.map((song) => {
      song.isAdded = false;
      return song;
    }));
  }, [props.searchResult]);

  const handleClick = (index, song) => {
    const newSongs = [...songs];
    newSongs[index].isAdded = true;
    setSongs(newSongs);
    props.addToQueue(song);

    setTimeout(() => {
      const revertedSongs = [...songs];
      revertedSongs[index].isAdded = false;
      setSongs(revertedSongs);
    }, 1000);
  }

  return (
    <div>
      <Header>
        <div>
          Search Results: <b>{props.searchValue}</b>
        </div>
        <div onClick={() => props.toQueue()}>
          <i className="fa fa-angle-right"></i> to Queue
        </div>
      </Header>
      <ScrollableContainer>
        {props.isLoading ? (
          <Loader>
            Loading ...
          </Loader>
        ) : (
          <div>
            {songs.map((song, index) => (
              <SingleSong className='single-song' key={index}>
              <div style={{display:'flex', justifyContent:'flex-start'}}>
                <Thumbnail src={song.thumbnail ? song.thumbnail : defaultThumbnail} alt="song-thumbnail" />
                <SongDetailContainer>
                  <div style={{fontSize:'1em'}}>{song.title}</div>
                  <div style={{fontSize:'0.6em'}}>{song.author}</div>
                </SongDetailContainer>
              </div>
              <ModButtonContainer>
                <div style={{
                  position: 'relative',
                  right: '-2em',
                  opacity: songs[index].isAdded ? '1':'0',
                  transition: '0.5s all ease-in-out',
                  transform: songs[index].isAdded ? 'translateX(-1em)' : '',
                  fontSize: '0.75em',
                  alignItems: 'center',
                  padding: '0.3em'
                }}>
                  Added
                </div>
                <ModButton src={addButton} onClick={() => handleClick(index, song)}/>
              </ModButtonContainer>
            </SingleSong>
            ))}
          </div>
        )}
      </ScrollableContainer>
    </div>        
  )
}

export default SearchResult;
