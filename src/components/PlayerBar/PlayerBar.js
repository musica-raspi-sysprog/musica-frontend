import React from 'react';
import styled, { keyframes } from 'styled-components';

import defaultThumbnail from './../../assets/default-thumbnail.jpeg';
import pauseButton from './../../assets/pause-button.svg';
import playButton from './../../assets/play-button.svg';
import loadingButton from './../../assets/loading-button.svg';
import {Thumbnail} from './../StyledComponents.js';

const StyledPlayerBarDiv = styled.footer`
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100vw;
  max-height: 100px;
  background-color: #00051c;
  color: #FFFFFF;
  display: flex;
  justify-content: space-between;
`

const CurrentSongContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  text-align: right;
`

const spinnerKeyframes = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(-360deg);
  }
`

const PlayerButton = styled.img`
  width: 3.5em;
  height: 3.5em;
  margin: 0 0.5em 0 0;
  -webkit-filter: invert(1); /* safari 6.0 - 9.0 */
  filter: invert(1);
  transform-origin: 50% 50%;

  &[data-animation="spinner"] {
    animation-name: ${spinnerKeyframes};
    animation-iteration-count: infinite;
    animation-duration: 1s;
    animation-timing-function: linear;
  }
`

const PlayerBar = ({song, skip, isPlaying, isLoading, togglePlay}) => {
  console.log(isPlaying, isLoading);
  const imagesPath = {
    "play": playButton,
    "loading": loadingButton,
    "pause": pauseButton,
  }

  const chooseImage = (isPlaying, isLoading) => {
    if (isPlaying && isLoading) {
      return imagesPath["loading"];
    } else if (isPlaying) {
      return imagesPath["pause"];
    } 
    return imagesPath["play"];
  }

  return (
    <StyledPlayerBarDiv>
      <div style={{display:'flex', flexDirection:'row', alignItems:'flex-end'}}>
        <Thumbnail 
          src={song && song.thumbnail ? song.thumbnail: defaultThumbnail}
          alt='Thumbnail'
          style={{marginRight:'0.5em'}}
        />
        <div style={{
          fontSize:'0.75em',
          display: "flex",
          flexFlow: "row nowrap",
        }} onClick={() => skip()}>
          <i className="fa fa-angle-right"></i>
          <i className="fa fa-angle-right"></i>
        </div>
      </div>
      <CurrentSongContainer>
        <div style={{display:'flex', flexDirection:'column', justifyContent:'center', padding:'0.2em'}}>
          <div style={{fontSize:'1.5em'}}>{song && song.title ? song.title : 'Song Title'}</div>
          <div style={{fontSize:'0.75em'}}>{song && song.author ? song.author : 'Song Author'}</div>
        </div>
        <div>
          <PlayerButton 
            src={chooseImage(isPlaying, isLoading)}
            alt='Music Button' 
            onClick={togglePlay}
            style={{margin:'0 1.5em 0 0.5em', width: '2.5em'}}
            data-animation={isPlaying && isLoading? "spinner": "idle"}
          />
        </div>
      </CurrentSongContainer>
    </StyledPlayerBarDiv>
  )
}

export default PlayerBar;