import React, { useEffect, useState, useReducer } from 'react';
import styled from 'styled-components';
import socketIOClient from 'socket.io-client';

import AppHeader from './components/Header/Header.js'
import PlayerBar from './components/PlayerBar/PlayerBar.js';
import Queue from './components/SongContainer/Queue/Queue.js';
import SearchBar from './components/SearchBar/SearchBar.js';
import SearchResult from './components/SongContainer/SearchResult/SearchResult.js';

import background from './assets/musica-bg.png';
// import { dummyQueue } from './components/StyledComponents.js';
const host = '192.168.4.1:4001'
const socket = socketIOClient(host);

const Main = styled.div`
  background-image: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.75) ), url(${background});
  background-size: cover;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: #F5F5F5;
`

const Title = styled.div`
  font-style: italic;
  font-family: Helvetica;
  font-size: 1.5em;
  margin: 0.5em;
  margin-top: -1.5em;
`

const SongContainer = styled.div`
  width: 90vw;
  background-color: rgba(0, 0, 0, 0.5);
`

function queueReducer(queue, action) {
  switch(action.type) {
    case 'pop':
      return queue.filter((_, index) => {
        return index !== 0;
      });
    case 'update':
      return action.newQueue;
    default:
      throw new Error();
  }
} 

const App = () => {
  const [isDisplayingQueue, setIsDisplayingQueue] = useState(true);
  const [searchResultIsLoading, setSearchResultIsLoading] = useState(true);
  const [queueIsLoading, setQueueIsLoading] = useState(true);
  const [searchResult, setSearchResult] = useState([]);
  const [searchValue, setSearchValue] = useState('');
  const [currentSong, setCurrentSong] = useState(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const [isDownloading, setIsDownloading] = useState(false);
  const [queue, queueDispatch] = useReducer(queueReducer, []);

  //function yang nerima kalo dikasi, queue skrg ganti sama queue itu kan itu kan kan kesana kemari membawa alamat jret jret

  
  useEffect(() => {
    socket.on('currentQueue', (state) => {
      setQueueIsLoading(true);
      console.log(state);

      setCurrentSong({...state.queue[0]});
      queueDispatch({
        type: "update",
        newQueue: state.queue.filter((_, index) => {
          return index !== 0;
        }),
      });
      setIsPlaying(state.currentlyPlaying);
      setQueueIsLoading(false);
      console.log(state.queue)
    });

    socket.on("downloading", (state) => {
      setIsDownloading(state.status);
      console.log("status", state);
    });

    socket.emit('currentQueue', '');
  }, []);

  const play = () => {
    socket.emit('play', '');
  }

  const pause = () => {
    socket.emit('pause', '');
  }

  const togglePlay = () => {
    if (isPlaying) {
      pause();
    } else {
      play();
    }
    setIsPlaying(!isPlaying);
  }

  const toQueue = () => {
    setIsDisplayingQueue(true);
  }

  const greet = () => {
    const date = new Date();
    const hour = date.getHours();
    if (hour < 12) {
        return "Good Morning";
    } else if (hour < 18) {
        return "Good Afternoon";
    } else {
        return "Good Evening";
    }
  }

  const playNextSong = () => {
    socket.emit('skip', '');
  }

  const updateSearch = (value) => {
    setIsDisplayingQueue(false);
    setSearchResultIsLoading(true);
    setSearchValue(value);
    if (value) {
      let url = `https://www.googleapis.com/youtube/v3/search?part=id,snippet&maxResults=25&q=${value}&type=video&key=AIzaSyCzGlvPZRfRDqdd62CHDR0CKsSams-coC8`;
      fetch(url)
        .then(res => res.json())
        .then(parsed => {
          setSearchResult(parsed.items
            .map(({id, snippet}) => ({
              "title": snippet.title,
              "author": snippet.channelTitle,
              "thumbnail": snippet.thumbnails.default.url,
              "id": id.videoId,
            })));
            setTimeout(() => {
              setSearchResultIsLoading(false);
            }, 250);
          });
    }

    // When you reach the daily limit to use youtube API
    // setIsDisplayingQueue(false);
    // setSearchResultIsLoading(false);
    // setSearchResult([...dummyQueue]);
  }

  return (
    <Main>
      <AppHeader/>
      <Title>
        {greet()}
      </Title>
      <SearchBar
        onSubmit={(value) => updateSearch(value)}
      />
      <SongContainer>
        { isDisplayingQueue 
          ? <Queue
              queue={queue}
              removeFromQueue={(songId) => socket.emit('removeQueue', songId)}
              isLoading={queueIsLoading}
              currentSong={currentSong}
            />
          : <SearchResult
              searchResult={searchResult}
              searchValue={searchValue}
              toQueue={() => toQueue()}
              addToQueue={(song) => socket.emit('addToQueue', song)}
              isLoading={searchResultIsLoading}
            />
        }
      </SongContainer>
      <PlayerBar
        song={currentSong}
        isPlaying={isPlaying}
        isLoading={isDownloading}
        togglePlay={togglePlay}
        skip={() => playNextSong()}
      />
    </Main>
  );
}

export default App;
